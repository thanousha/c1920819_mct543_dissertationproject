# 1     Imports
from flask import Flask, render_template, url_for, request, redirect, flash, abort
import csv

app = Flask(__name__)

# 2.	Calculating the fuel poverty risk score:
# 2a.	Weighting each variable 
weight_income = 4
weight_household_size = 2
weight_property_type = 1
weight_tenure_type = 1
weight_epc_band = 2

# 2b.	Normalising the data for each variable
def income_score(median_income):
   median_income = int(median_income)
   if median_income < 15000:
      return 7
   elif 15001 <= median_income <= 30000:
      return 6
   elif 30001 <= median_income <= 45000:
      return 5
   elif 45001 <= median_income <= 60000:
      return 4
   elif 60001 <= median_income <= 75000:
      return 3
   elif 75001 <= median_income <= 90000:
      return 2
   elif median_income > 90000:
      return 1

# no function needed for household size score, as size corresponds to the score

def property_type_score(property_type):
   if property_type == "Converted flat":
      return 6
   elif property_type == "End terrace":
      return 5
   elif property_type == "Mid terrace":
      return 4
   elif property_type == "Semi-detached":
      return 3
   elif property_type == "Purpose-built flat":
      return 2
   elif property_type == "Detached":
      return 1  

def tenure_type_score(tenure_type):
   if tenure_type == "Private rented":
      return 5
   elif tenure_type == "Local authority":
      return 4
   elif tenure_type == "Housing association":
      return 3
   elif tenure_type == "Owner occupied: Own with mortgage":
      return 2
   elif tenure_type == "Owner occupied: Own outright":
      return 1
   
def epc_band_score(epc_band):
   if epc_band == 'A' or epc_band == 'B' or epc_band == 'C':
      return 1
   elif epc_band == 'D':
      return 2
   elif epc_band == 'E':
      return 3
   elif epc_band == 'F' or epc_band =='G':
      return 4

# 2c.	Returning a total score by adding together the weight multiplied by the score for each variable
def calculate_fps(median_income, household_size, property_type, tenure_type, epc_band
):
    total_score = sum([
    weight_income * income_score(median_income),
    weight_household_size * household_size,
    weight_property_type * property_type_score(property_type),
    weight_tenure_type * tenure_type_score(tenure_type),
    weight_epc_band * epc_band_score(epc_band),
    ])
    return total_score

def calculate_level(total_score):
   if total_score >= 10 and total_score <= 25:
      return "Low Risk"
   elif total_score >= 26 and total_score <= 40:
      return "Medium Risk"
   elif total_score >= 41 and total_score <= 57:
      return "High Risk"
   else:
      return "Invalid Score, try again"
   
def calculate_image(total_score):
   if total_score >= 10 and total_score <= 25:
      return "static/img/risklow.png"
   elif total_score >= 26 and total_score <= 40:
      return "static/img/riskmed.png"
   elif total_score >= 41 and total_score <= 57:
      return "static/img/riskhigh.png"
   else:
      return "Invalid Score, try again"
   
# 3.	Returning the fuel poverty risk score and saving user inputs to a CSV file
@app.route("/", methods=['GET', 'POST'])
def calc():
   score = None
   risk_image=None
   if request.method == 'POST':
     median_income = int(request.form['median_income'])
     household_size = int(request.form['household_size'])
     property_type = request.form['property_type']
     tenure_type = request.form['tenure_type']
     epc_band = request.form['epc_band']

     with open ('user_inputs.csv', mode = 'a', newline ='') as file:
        writer = csv.writer(file)
        writer.writerow([median_income, household_size, property_type, tenure_type, epc_band])
     total_score = calculate_fps (median_income, household_size, property_type, tenure_type, epc_band)
     score = calculate_level(total_score)
     risk_image = calculate_image(total_score)

   return render_template('calc.html', title='Home', score=score, risk_image=risk_image)

@app.route("/")
@app.route("/article")
def article():
   return render_template('article.html', title='Article')

@app.route("/datastory")
def datastory():
   return render_template('datastory.html', title='Data Story')

@app.route("/index")
def index():
   return render_template('index.html', title='Home')

@app.route("/scrolly")
def scrolly():
   return render_template('scrolly.html', title='Scrolly')
  
if __name__ == '__main__':
   app.run(debug=True)

